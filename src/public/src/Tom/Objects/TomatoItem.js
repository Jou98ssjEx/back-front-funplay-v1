class TomatoItem extends Phaser.Physics.Arcade.StaticGroup {
    constructor(config) {
        super(config.physicsWorld, config.scene);
        // setInterval(() => {

        // }, 10);
        this.addTomatoItem();
    }

    addTomatoItem() {
        this.create(
            Phaser.Math.Between(70, this.scene.scale.width - 70),
            Phaser.Math.Between(180, this.scene.scale.height - 100),
            'tomato_item'
        );
    }

    destroyItem() {
        this.children.entries[0].destroy();
        this.addTomatoItem();
    }

}

export default TomatoItem;