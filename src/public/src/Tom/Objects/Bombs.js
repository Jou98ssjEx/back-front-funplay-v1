class Bombs extends Phaser.Physics.Arcade.Group {
    constructor(config) {
        super(config.physicsWorld, config.scene);
        this.addBomb();
    }

    addBomb() {
        this.create(
                Phaser.Math.Between(20, this.scene.scale.width - 20), 10, 'bomb')
            .setScale(1.3)
            .setDepth(2)
            .setBounce(1)
            .setCircle(18)
            .setVelocityX(
                (Phaser.Math.Between(0, 1)) ? 100 : -100
            )
            .setGravityY(-1500);
    }

    update() {
        this.children.iterate(bomb => {
            if (bomb.body.velocity.x < 0) {
                bomb.setAngularVelocity(-300);
            } else {
                bomb.setAngularVelocity(300);
            }
        });
    }
}

export default Bombs;