import BootLoader from '/src/Tom/Bootloader.js';
// import Load from '/src/Tom/scenes/Load.js';
import UI from '/src/Tom/scenes/UI.js';
import Menu from '/src/Tom/scenes/Menu.js';
import Play from '/src/Tom/scenes/Play.js';

// import Playing from '/src/scenes/Playing.js';


const lienzo = document.getElementById('ingreso');
// 1295
// 792

const config = {
    width: 940,
    height: 440,
    parent: "ingreso",
    // background: ruta,
    type: Phaser.AUTO,
    pixelArt: true,
    scene: [
        BootLoader,
        Play,
        UI,
        Menu,

    ],
    physics: {
        default: "arcade",
        arcade: {
            gravity: {
                // y: 500
                y: 2000
            },
            // debug: true
        }
    },
    scale: {
        mode: Phaser.ScaleModes.SHOW_ALL
            // mode: Phaser.Scale.FIT,
            // autoCenter: Phaser.Scale.CENTER_BOTH
    },
    dom: {
        // createContainer: truek
    },
}

const game = new Phaser.Game(config);
export default game;