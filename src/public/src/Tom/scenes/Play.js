import Tomato from '/src/Tom/Player/Tomato.js';
import Bombs from '/src/Tom/Objects/Bombs.js';
import TomatoItem from '/src/Tom/Objects/TomatoItem.js';

class Play extends Phaser.Scene {
    constructor() {
        super({ key: 'Play' });
    }

    init() {
        console.log('Se ha iniciado la escena Play');
        this.scene.launch('UI');
        this.STATE_GAME_PLAYING = 1;

        this.STATE_GAME_PLAYINGIZQ = 5;
        this.STATE_GAME_PLAYINGDERE = 6;
        this.STATE_GAME_PLAYINGP = 7;

        this.stateGame = this.STATE_GAME_PLAYING;

        // console.log();
    }

    create() {
        this.add.image(0, 0, 'background')
            .setOrigin(0);

        this.dere = this.add.image(800, this.scale.height / 2, 'btnR');
        this.dere.setTint(0xfff);
        this.dere.setInteractive({ useHandCursor: true });
        this.dere.setScale(1.5);

        this.izq = this.add.image(120, this.scale.height / 2, 'btnR');
        this.izq.setTint(0xfff);
        this.izq.setInteractive({ useHandCursor: true });
        this.izq.setScale(1.5);
        this.izq.flipX = true;

        this.arriba = this.add.image(70, this.scale.height / 2, 'btnR');
        this.arriba.setInteractive({ useHandCursor: true });
        this.arriba.flipX = true;
        this.arriba.setTint(0xfff);
        this.arriba.setScale(1.5);
        this.arriba.setRotation(1.5708);

        this.arriba1 = this.add.image(860, this.scale.height / 2, 'btnR');
        this.arriba1.setInteractive({ useHandCursor: true });
        this.arriba1.flipX = true;
        this.arriba1.setTint(0xfff);
        this.arriba1.setScale(1.5);
        this.arriba1.setRotation(1.5708);



        this.wall_floor = this.physics.add.staticGroup();

        this.wall_floor.create(0, 0, 'wall')
            .setOrigin(0);
        this.wall_floor.create(this.scale.width, 0, 'wall')
            .setOrigin(1, 0)
            .setFlipX(true);

        this.wall_floor.create(-13, this.scale.height, 'floor')
            .setOrigin(0, 1);

        this.wall_floor.refresh();

        this.wall_floor.getChildren()[2].setOffset(0, 15);

        this.btnCAM = this.add.image(880, 420, 'btnCAM').setInteractive({ useHandCursor: true });
        // fullscreen
        const fullscreen = (e) => {
                if (e.webkitRequestFullScreen) {
                    e.webkitRequestFullScreen();
                } else if (e.mozRequestFullScreen) {
                    e.mozRequestFullScreen();
                }
            }
            // Botn para el fullScreen
        this.btnCAM.on('pointerdown', function(pointer) {
            this.setTint(0x4e9ea5);
        });
        this.btnCAM.on('pointerout', function(pointer) {
            this.clearTint();
        });
        this.btnCAM.on('pointerup', () => {
            fullscreen(document.getElementById('ingreso'));
        });


        // Bombs
        this.bombsGroup = new Bombs({
            physicsWorld: this.physics.world,
            scene: this
        });

        // Items
        this.itemsGroup = new TomatoItem({
            physicsWorld: this.physics.world,
            scene: this
        });

        // Personaje
        this.tomato = new Tomato({
            scene: this,
            x: 100,
            y: 100,
        });

        this.physics.add.collider([this.tomato, this.bombsGroup], this.wall_floor);
        this.physics.add.overlap(this.tomato, this.bombsGroup, () => {
            this.tomato.bombCollision();
        });

        this.physics.add.overlap(this.itemsGroup, this.tomato, () => {
            this.sound.play('pop');
            this.registry.events.emit('update_points');
            this.itemsGroup.destroyItem();
            this.bombsGroup.addBomb();
        });

        // this.btnR = this.input.keyboard.createCursorKeys();
        // this.btnR.on(Phaser.Input.Events.POINTER_DOWN, () => {
        //     // alert('HOLA');

        // });

        this.izq.on(Phaser.Input.Events.POINTER_DOWN, () => {

            this.stateGame = this.STATE_GAME_PLAYINGIZQ;
            // this.hitMan(1);
        });
        this.izq.on(Phaser.Input.Events.POINTER_UP, () => {
            // this.stateGame = this.STATE_GAME_PLAYINGP;
            this.stateGame = this.STATE_GAME_PLAYING;


        });

        this.dere.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.stateGame = this.STATE_GAME_PLAYINGDERE;

            // this.hitMan(-1);

        });
        this.dere.on(Phaser.Input.Events.POINTER_UP, () => {
            // this.stateGame = this.STATE_GAME_PLAYINGP;
            this.stateGame = this.STATE_GAME_PLAYING;

        });


        this.arriba.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.stateGame = this.STATE_GAME_PLAYINGP;

            // this.hitMan(-1);

        });
        this.arriba.on(Phaser.Input.Events.POINTER_UP, () => {
            // this.stateGame = this.STATE_GAME_PLAYINGP;
            this.stateGame = this.STATE_GAME_PLAYING;

        });
        this.arriba1.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.stateGame = this.STATE_GAME_PLAYINGP;

            // this.hitMan(-1);

        });
        this.arriba1.on(Phaser.Input.Events.POINTER_UP, () => {
            // this.stateGame = this.STATE_GAME_PLAYINGP;
            this.stateGame = this.STATE_GAME_PLAYING;

        });

    }

    update() {
        this.tomato.update();
        this.bombsGroup.update();

        switch (this.stateGame) {
            case this.STATE_GAME_PLAYINGIZQ:
                this.tomato.MovIZQ();
                break;
            case this.STATE_GAME_PLAYINGDERE:
                this.tomato.MovDERE();
                break;
            case this.STATE_GAME_PLAYINGP:
                this.tomato.MovARR();
                break;
        }
        // if (this.btnR.isDown) {
        //     this.tomato.setVelocityX(200);
        // }
    }
}

export default Play;