class Playing extends Phaser.Scene {
    constructor() {
        super('Playing');
    }
    init() {
        // Variables Generales
        // saber que no hemos dado un click para  comenzar el juego
        this.f = false;

        // guarda los damantes recolectados
        this.dianmantesSaves = 0;

        // Numero de diamantes que van aparecer
        // this.DIAMANTESTOTALES = 2;
        this.DIAMANTESTOTALES = Phaser.Math.Between(15, 30);

        // BURBUJAS
        this.BURBUJAS = Phaser.Math.Between(15, 30);

        // Dejar sin movimiento al personaje
        this.endGame = false;

        // Tiempo
        // this.totalTime = 30;

        // Cronometro del Tiempo restante
        // El tiempo tiene que se igual ex: deley 20000 equivale a 20 segundos
        this.tiempoDeley = 30000;
        this.tiempoSegundos = 30;

        //////////////////////////////
        this.actual_points = 0;
    }
    preload() {
        console.log('Inicio del juego');
    }
    create() {
        // Mostrar Background
        this.back = this.physics.add.image(0, 0, 'bg');
        this.back.setOrigin(0, 0);

        // Personajes secundarios
        this.booble = [];
        for (var i = 0; i < this.BURBUJAS; i++) {
            var XB = Phaser.Math.Between(1, 1200);
            var YB = Phaser.Math.Between(450, 1000);
            this.booble1 = this.add.image(XB, YB, 'booble' + Phaser.Math.Between(1, 2));
            this.booble1.vel = 0.2 + Phaser.Math.Between(0, 2);
            this.booble1.alpha = 0.9;
            // this.booble1.setScale(Phaser.Math.Between(0, 2, 1, 2));
            // console.log('b: ' + this.booble1.vel);
            this.booble[i] = this.booble1;
            // console.log('bur: ' + this.booble);
        }
        // this.booble2 = this.add.image(100, 300, 'booble2');

        this.mollusk = this.add.image(400, 300, 'mollusk');
        this.shark = this.add.image(300, 30, 'shark');
        this.fishes = this.add.image(100, 300, 'fishes');


        // Personaje
        this.ca = this.physics.add.image(100, 100, 'ca');

        // AL dar un click el personaje seguirar el puntero
        this.input.on('pointerdown', () => {
            this.fa();
        });

        // Diamantes
        var atlasTexture = this.textures.get('diamantes');

        var frames = atlasTexture.getFrameNames();
        console.log(frames);
        this.imagesA = [];

        // Ciclo para crear los diamantes
        for (var i = 0; i < this.DIAMANTESTOTALES; i++) {
            this.imgS = this.physics.add.sprite(200, 100, 'diamantes', frames[Phaser.Math.Between(0, 3)]);
            // this.imgS.setScale(0.50 + Phaser.Math.Between(0, 1));
            this.imgS.x = Phaser.Math.Between(50, 900);
            this.imgS.y = Phaser.Math.Between(50, 400);
            // this.imgS.width = 32;
            // this.imgS.height = 32;

            this.imagesA[i] = this.imgS;

            // console.log(this.imagesA);

            var rectanguloDiamante = this.getBoundsDiamante(this.imgS);
            var rectHorse = this.getBoundsDiamante(this.ca);
            // console.log(rectanguloDiamante);

            // Comparacion para que no choquen los diamantes ni el personaje
            while (this.isOverlappingOtherRectangle(i, rectanguloDiamante) || this.isRentangleOverlapping(rectHorse, rectanguloDiamante)) {
                this.imgS.x = Phaser.Math.Between(50, 900);
                this.imgS.y = Phaser.Math.Between(50, 400);
                rectanguloDiamante = this.getBoundsDiamante(this.imgS);
                console.log('Bucle');
            }

        }

        // Botones circulares
        // this.btn = this.add.image(40, 410, "btn").setInteractive({ useHandCursor: true });
        // this.btn1 = this.add.image(100, 410, "btn1").setInteractive({ useHandCursor: true });
        // this.btn2 = this.add.image(160, 410, "btn2").setInteractive({ useHandCursor: true });
        this.btn3 = this.add.image(880, 410, "btn3").setInteractive({ useHandCursor: true });

        // Cambiar de escena
        this.btn3.on('pointerup', () => {
            this.scene.start('Menu', 'fin');
        });
        // Selecion dentro y fuera del canvas
        var graphics = this.add.graphics();

        graphics.fillStyle(0x000000, 0.5);
        graphics.fillRect(0, 0, 940, 440);
        graphics.setVisible(false);

        this.input.on('gameout', function() {
            graphics.setVisible(true);
        });
        this.input.on('gameover', function() {
            graphics.setVisible(false);
        });


        // Mantener el objeto dentro del canvas
        this.ca.setCollideWorldBounds(true);

        // Grupo de explosion
        this.explosionGroup = this.add.group();

        // Agg explosion
        this.explosion = this.add.sprite(100, 100, 'explosion');
        // console.log('Exploasion:' + this.explosion);
        // Animacion de la exposion
        this.explosion_tweenScale = this.tweens.add({
            targets: [this.explosion],
            ease: 'Power1'
        });

        this.explosion_Alpha = this.tweens.add({
            targets: [this.explosion],
            alpha: 0
        });
        this.explosion.visible = false
            // this.explosion.kill();

        // Variable para comenzar el puntaje
        this.currentScore = 0;

        // Estilos general
        this.style = {
            color: '#000',
            // fontSize: 30,
            font: 'bold 30pt Arial'
        }

        // Puntos textos
        this.scoreText = this.add.text(this.sys.game.config.width / 2, 20, '0', this.style);


        // Tiempo en disminucion
        this.timerText = this.add.text(10, 20, '0', this.style);

        // Evento del tiempo
        // Phaser.Time.TimerEvent()
        this.timerC = this.time.addEvent({
            // delay: 20000, // ms
            delay: this.tiempoDeley, // ms
            callback: () => {
                if (this.timerC.getOverallProgress() == 1) {
                    this.endGame = true;
                    this.timerC.remove();
                    // this.timerC.paused = true;
                    console.log('Fin del Juego');
                    // alert('cero');
                    this.gamerOverFin();
                }
            },
            //args: [],
            callbackScope: this,
            paused: true
                // loop: false
        });

        // Si el tiempo esta pausado, cuando demos click se iniciara
        if (this.timerC.paused == true) {
            this.input.on('pointerdown', () => {
                this.fa();
                this.timerC.paused = false;
            });
            // alert('llamo a fa');
        }
    }

    // Pantalla de perdiste
    gamerOverFin() {
        this.centroW = this.game.config.width / 2;
        this.centroH = this.game.config.height / 2;
        let over = this.add.image(this.sys.game.config.width / 2, this.sys.game.config.height / 2, 'tabla').setScale(4);
        let finGAme = this.add.text(this.sys.game.config.width / 2 - 180, this.sys.game.config.height / 2 - 60, 'Perdiste', this.style);

        this.bJugar1 = this.physics.add.image(this.centroW - 120, this.centroH + 90, 'boton_Jugar');
        this.bSalir1 = this.physics.add.image(this.centroW + 120, this.centroH + 90, 'boton_Salir');

        this.bJugar1.setInteractive({ useHandCursor: true });
        this.bSalir1.setInteractive({ useHandCursor: true });
        this.bJugar1.on('pointerdown', function(pointer) {
            this.setTint(0xff0000);
        });
        // // Cuando el usuario abandona el boton
        this.bJugar1.on('pointerout', function(pointer) {
            this.clearTint();
        });

        // // Con esta funcion cambias la escena a traves del evento del boton ---Actualiza
        this.bJugar1.on('pointerup', () => {
            this.scene.start('Playing', { points: this.currentScore });
        });
        this.bSalir1.on('pointerdown', function(pointer) {
            this.setTint(0xff0000);
        });
        // // Cuando el usuario abandona el boton
        this.bSalir1.on('pointerout', function(pointer) {
            this.clearTint();
        });

        // // Con esta funcion cambias la escena a traves del evento del boton ---Actualiza
        // this.bSalir1.on('pointerup', () => {
        //     this.scene.start('Menu', { points: this.currentScore });
        // });

        this.bSalir1.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.add.tween({
                // targets: this.cp,
                // ease: 'Bounce.easeIn',
                // y: -200,
                // duration: 1000,
                onComplete: () => {
                    // this.scene.start('Play');
                    this.scene.start('Menu', { points: this.currentScore });
                }
            });
        });



        let contenedorF = this.add.container(0, -400);
        contenedorF.add([over, finGAme, this.bJugar1, this.bSalir1]);

        this.tweens.add({
            targets: contenedorF,
            ease: 'Pointer1',
            y: 0
        });

    }

    // Tabla para finalizar: Puntaje y otras cosas
    tablaDatos() {
        // Variables para saber el ancho y alto de canvas o para centrar
        this.centroW = this.game.config.width / 2;
        this.centroH = this.game.config.height / 2;

        let s = this.add.image(this.centroW, this.centroH, 'tabla').setScale(4);

        let prePuntos = this.add.text(this.centroW - 180, this.centroH - 60, 'Puntos:', this.style);
        let puntos = this.add.text(this.centroW + 10, this.centroH - 60, this.currentScore, this.style);

        this.bJugar = this.physics.add.image(this.centroW - 50, this.centroH + 90, 'boton_Jugar');
        this.bSalir = this.physics.add.image(this.centroW + 200, this.centroH + 90, 'boton_Salir');

        this.bJugar.setInteractive({ useHandCursor: true });
        this.bSalir.setInteractive({ useHandCursor: true });
        this.bJugar.on('pointerdown', function(pointer) {
            this.setTint(0xff0000);
        });
        // // Cuando el usuario abandona el boton
        this.bJugar.on('pointerout', function(pointer) {
            this.clearTint();
        });

        // // Con esta funcion cambias la escena a traves del evento del boton ---Actualiza
        this.bJugar.on('pointerup', () => {
            this.scene.start('Playing', 'fin');
        });
        this.bSalir.on('pointerdown', function(pointer) {
            this.setTint(0xff0000);
        });
        // // Cuando el usuario abandona el boton
        this.bSalir.on('pointerout', function(pointer) {
            this.clearTint();
        });

        // // Con esta funcion cambias la escena a traves del evento del boton ---Actualiza
        this.bSalir.on('pointerup', () => {
            this.scene.start('Menu', 'fin');
        });




        let contenedor = this.add.container(0, -400);
        contenedor.add([s, prePuntos, puntos, this.bJugar, this.bSalir]);

        this.tweens.add({
            targets: contenedor,
            ease: 'Pointer1',
            y: 0
        });
    }

    // Incremeta en puntos los diamantes recolectado, y si los recoges todo termina el juego
    incrementScore() {
        this.currentScore += 100;
        this.scoreText.text = this.currentScore;

        this.dianmantesSaves += 1;

        if (this.dianmantesSaves >= this.DIAMANTESTOTALES) {
            this.endGame = true;
            this.tablaDatos();
            this.timerC.paused = true;
            if (this.timerC.paused == true) {
                this.input.on('pointerdown', () => {
                    this.fa();
                    // this.timerC.paused = false;
                    this.timerC.remove();

                });
            }
            console.log('Fin del juego');
        }
    }

    // Si damos un click para comensar el juego, con esto lo sabremos
    fa() {
        if (this.f) {
            this.tweensMollusk = this.tweens.add({
                targets: [this.mollusk],
                props: {
                    y: {
                        value: -0.001,
                        duration: 5800
                    }
                },
                ease: 'Power1',
                yoyo: true,
                loop: true
            });
        }
        this.f = true;
    }

    // Crear el Bounds del Diamante
    getBoundsDiamante(rectDiamante) {
        return new Phaser.Geom.Rectangle(rectDiamante.x, rectDiamante.y, rectDiamante.width, rectDiamante.height);
    }

    // Comparar los 2 rectangulos
    isRentangleOverlapping(rectA, rectB) {
        if (rectA.x > rectB.x + rectB.width || rectB.x > rectA.x + rectA.width) {
            return false;
        }

        if (rectA.y > rectB.y + rectB.height || rectB.y > rectA.y + rectA.height) {
            return false;
        }

        return true;
    }

    // Hace un proceso raro donde tienes que analisarlo para entenderlo
    isOverlappingOtherRectangle(index, rectB) {

        for (var i = 0; i < index; i++) {
            var rectangulo1 = this.getBoundsDiamante(this.imagesA[i]);
            if (this.isRentangleOverlapping(rectangulo1, rectB)) {
                return true;
            }
        }
        return false;
    }

    // Crear los limites del personaje
    getBoundsHorse() {
        var x0 = this.ca.x - Math.abs(this.ca.width) / 4;
        var width1 = Math.abs(this.ca.width) / 2;
        var y0 = this.ca.y - this.ca.height / 2;
        var heigth1 = this.ca.height;

        return new Phaser.Geom.Rectangle(x0, y0, width1, heigth1);
    }


    update(time, delta) {

        // Comparamos para saber cuando le vamos a dar click para q comience el juego y algo mas
        if (this.f && !this.endGame) {

            for (var i = 0; i < this.BURBUJAS; i++) {
                var BUR = this.booble[i];
                BUR.y -= BUR.vel;
                if (BUR.y < -50) {
                    BUR.y = 600;
                    BUR.x = Phaser.Math.Between(1, 1200);
                }
            }

            // Movimientos de personajes secundarios
            this.shark.x--;
            if (this.shark.x < -300) {
                this.shark.x = 1200;
            }

            this.fishes.x += 0.3;
            if (this.fishes >= 1300) {
                this.fishes.x = -300;
            }


            var pointerX = this.input.x;
            var pointerY = this.input.y;

            // console.log('x:' + pointerX);
            // console.log('y:' + pointerY);

            var distX = pointerX - this.ca.x;
            var distY = pointerY - this.ca.y;

            if (distX > 0) {
                // this.ca.scale(1, 1);
                this.ca.flipX = false;
            } else {
                // this.ca.scale(-1, 1);
                this.ca.flipX = true;
            }

            this.ca.x += distX * 0.02;
            this.ca.y += distY * 0.02;


            // La colison del personaje con los diamantes
            for (var i = 0; i < this.DIAMANTESTOTALES; i++) {
                var rectHorse = this.getBoundsHorse()
                var rectDiamnate = this.getBoundsDiamante(this.imagesA[i]);

                if (this.imagesA[i].visible && this.isRentangleOverlapping(rectHorse, rectDiamnate)) {
                    console.log('Colision');

                    this.s = this.sound.add("pum", { loop: false });
                    this.s.play();

                    this.incrementScore();
                    this.imagesA[i].visible = false;

                    // Explosion
                    this.explosion.visible = true;
                    this.explosion.x = this.imagesA[i].x;
                    this.explosion.y = this.imagesA[i].y;
                    this.explosion_tweenScale.resume();
                    this.explosion_Alpha.resume();
                }

            }
        }

        // Tiempo restante del juego --- Cronometro
        // this.timerText.setText('Time: ' + Math.floor(20 - this.timerC.getElapsedSeconds()));
        this.timerText.setText('Time: ' + Math.floor(this.tiempoSegundos - this.timerC.getElapsedSeconds()));
        // console.log('Time: ' + this.timerC.getElapsedSeconds());
        // console.log('f: ' + this.timerC.getOverallProgress());
    }
}
export default Playing;