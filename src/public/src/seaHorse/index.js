import BootLoader from '/src/seaHorse/Bootloader.js';
import Load from '/src/seaHorse/scenes/Load.js';
import Menu from '/src/seaHorse/scenes/Menu.js';
import Playing from '/src/seaHorse/scenes/Playing.js';

// import Playing from '/src/scenes/Playing.js';


const lienzo = document.getElementById('ingreso');
// 1295
// 792

const config = {
    width: 940,
    height: 440,
    parent: "ingreso",
    // background: ruta,
    type: Phaser.AUTO,
    // pixelArt: true,
    scene: [
        BootLoader,
        Load,
        Menu,
        Playing,

    ],
    physics: {
        default: "arcade",
        arcade: {
            gravity: {
                // y: 500
            },
            // debug: true
        }
    },
    scale: {
        mode: Phaser.ScaleModes.SHOW_ALL
    }
}

const game = new Phaser.Game(config);
export default game;