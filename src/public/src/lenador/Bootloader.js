class Bootloader extends Phaser.Scene {
    constructor() {
        super('Bootloader');
    }

    preload() {
        // Ruta de las imagenes
        this.load.path = '/assets/play3/assets/';
        this.load.image([
            'background',
            'man_stand',
            'man_hit',
            'trunk',
            'tomb',
            // 'buttonPlay',
            'tronco',
            'cp',
            'title',
            'man_hit_marco',
            'btnCAM',
            'btnCEx',
            'btnR',
        ]);

        this.load.image('font', '../font/font.png');
        this.load.json('fontData', '../font/font.json');

        this.load.audio('loopMusic', '../sounds/musicLoop.mp3');
        this.load.audio('sfxHit', '../sounds/sfxHit.mp3');
        this.load.audio('sfxGameOver', '../sounds/sfxGameOver.mp3');

        this.load.on('complete', () => {

            this.sound.play('loopMusic', { loop: false });

            const fontData = this.cache.json.get('fontData');
            this.cache.bitmapFont.add('pixelFont', Phaser.GameObjects.RetroFont.Parse(this, fontData));

            this.scene.start('Menu');

        });

    }

}
export default Bootloader;