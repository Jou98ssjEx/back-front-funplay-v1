class Playing extends Phaser.Scene {
    constructor() {
        super('Playing');
    }
    init() {
        // Variables Generales

        this.STATE_GAME_NONE = 0;
        this.STATE_GAME_LOADING = 1;
        this.STATE_GAME_PLAYING = 2;
        this.STATE_GAME_GAME_OVER = 3;
        this.STATE_GAME_WIN = 4;
        this.STATE_GAME_PLAYINGIZQ = 5;
        this.STATE_GAME_PLAYINGDERE = 6;
        this.STATE_GAME_PLAYINGP = 7;
        this.stateGame = this.STATE_GAME_NONE;

        this.distanceTrunks = 60;

        this.cursors = this.input.keyboard.createCursorKeys();
        this.pressEnable = true;

        this.actual_points = 0;
    }
    preload() {
        console.log('Inicio del juego');
    }
    create() {
        this.bg = this.add.sprite(0, 0, 'background');
        this.bg.setOrigin(0);
        this.tronco = this.add.sprite(480, 210, 'tronco');
        this.man = this.add.sprite(385, 350, 'man_stand');
        this.tomb = this.add.sprite(385, 380, 'tomb');
        this.tomb.visible = false;

        this.dere = this.add.image(800, this.scale.height / 2, 'btnR');
        this.dere.setInteractive({ useHandCursor: true });
        this.dere.setScale(1.5);

        this.izq = this.add.image(120, this.scale.height / 2, 'btnR');
        this.izq.setInteractive({ useHandCursor: true });
        this.izq.setScale(1.5);
        this.izq.flipX = true;

        this.izq.on(Phaser.Input.Events.POINTER_DOWN, () => {

            this.stateGame = this.STATE_GAME_PLAYINGIZQ;
            // this.hitMan(1);
        });
        this.izq.on(Phaser.Input.Events.POINTER_UP, () => {
            this.stateGame = this.STATE_GAME_PLAYINGP;
            this.stateGame = this.STATE_GAME_PLAYING;
        });

        this.dere.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.stateGame = this.STATE_GAME_PLAYINGDERE;
        });
        this.dere.on(Phaser.Input.Events.POINTER_UP, () => {
            this.stateGame = this.STATE_GAME_PLAYINGP;
            this.stateGame = this.STATE_GAME_PLAYING;
        });

        this.points = this.add.bitmapText(
            this.scale.width / 2 - 340,
            80,
            'pixelFont',
            'PUNTOS  '
        ).setDepth(2).setOrigin(0.5);

        this.bar = this.add.rectangle(0, 0, this.scale.width, 20, 0x4e9ea5);
        this.bar.setOrigin(0);
        this.trunk = this.add.group();

        for (var i = 0; i < 30; i++) {
            this.trunk1 = this.trunk.create(Phaser.Math.Between(100, 800), Phaser.Math.Between(10, 300), 'trunk');
            this.trunk.killAndHide(this.trunk1);
        }

        this.startGAme();

    }

    refreshBar(value) {
        var newWidth = this.bar.width + value;
        if (newWidth > this.scale.width) {
            newWidth = this.scale.width;
        }
        if (newWidth <= 0) {
            newWidth = 0;
            this.gameOver();
        }
        this.bar.width = newWidth;
    }

    startGAme() {
        this.currentScore = 0;
        this.stateGame = this.STATE_GAME_PLAYING;
        console.log('cki');

        this.sequence = [];
        for (var o = 0; o < 10; o++) {
            this.addTrunk();
        }
        this.printSequence();
    }

    increaseScore() {
        this.actual_points += 100;
        this.points.text = `PUNTOS ${this.actual_points}`;

    }

    addTrunk() {
        this.refreshBar(6);
        this.number = Phaser.Math.Between(-1, 1);
        console.log('nume: ' + this.number);

        if (this.number == 1) {
            //Right
            this.tk = this.trunk.create(this.scale.width / 2 + 90, 280 - (this.sequence.length) * this.distanceTrunks, 'trunk');
            this.tk.direction = 1;
            // console.log('Ejecuto');
            this.tk.flipX = false;
            this.tk.setVisible(true);
            this.sequence.push(this.tk);
        } else
        if (this.number == -1) {
            //Left
            this.tk = this.trunk.create(this.scale.width / 2 - 70, 280 - (this.sequence.length) * this.distanceTrunks, 'trunk');
            this.tk.direction = -1;
            this.tk.flipX = true;
            this.tk.setVisible(true);
            this.sequence.push(this.tk);
        } else {
            //Nothing
            this.sequence.push(null);
            // console.log('Secuencia: ' + this.sequence);
        }

    }

    hitMan(direction) {
        this.sound.play('sfxHit');

        for (var i = 0; i < this.sequence.length; i++) {
            if (this.sequence[i] != null) {
                this.sequence[i].y += this.distanceTrunks;
            }
        }

        var firstTrunk = this.sequence.shift();
        console.log(firstTrunk);
        if (firstTrunk != null) {
            firstTrunk.destroy();
        }

        this.addTrunk();

        //Check GameOver
        var checkTrunk = this.sequence[0];
        if (checkTrunk != null && checkTrunk.direction == direction) {
            this.gameOver();
            console.log('perdio');
        } else {
            this.increaseScore();
        }
        this.printSequence();
    }
    gameOver() {
        this.stateGame = this.STATE_GAME_GAME_OVER;

        this.sound.play('sfxGameOver');

        this.man.visible = false;
        this.tomb.visible = true;
        this.tomb.x = this.man.x;

        this.scene.start('Menu', { points: this.actual_points });
    }

    printSequence() {
        var stringSequence = "";
        for (var i = 0; i < this.sequence.length; i++) {
            if (this.sequence[i] == null) {
                stringSequence += "0,";
            } else {
                stringSequence += this.sequence[i].direction + ",";
            }
        }
        console.log(stringSequence);
    }

    update(time, delta) {

        switch (this.stateGame) {
            case this.STATE_GAME_NONE:

                break;
            case this.STATE_GAME_LOADING:

                break;
            case this.STATE_GAME_PLAYING:

                this.refreshBar(-0.5);

                if (this.cursors.left.isDown && this.pressEnable) {
                    this.pressEnable = false;
                    console.log('left');
                    this.man.x = 385;
                    this.man.flipX = false;
                    this.man.setTexture('man_hit');
                    this.hitMan(-1);
                }

                if (this.cursors.right.isDown && this.pressEnable) {
                    this.pressEnable = false;
                    console.log('right');
                    this.man.x = 570;
                    this.man.flipX = true;
                    this.man.setTexture('man_hit');
                    this.hitMan(1);
                }

                if (this.cursors.left.isUp && this.cursors.right.isUp) {
                    this.pressEnable = true;
                    this.man.setTexture('man_stand');
                }
                break;
            case this.STATE_GAME_GAME_OVER:
                console.log("GAME OVER");
                break;
            case this.STATE_GAME_WIN:

                break;

            case this.STATE_GAME_PLAYINGIZQ:
                this.refreshBar(-0.5);
                if (this.pressEnable) {
                    this.pressEnable = false;
                    console.log('left');
                    this.man.x = 385;
                    this.man.flipX = false;
                    this.man.setTexture('man_hit');
                    this.hitMan(-1);
                }
                break;

            case this.STATE_GAME_PLAYINGDERE:
                this.refreshBar(-0.5);
                if (this.pressEnable) {
                    this.pressEnable = false;
                    console.log('right');
                    this.man.x = 570;
                    this.man.flipX = true;
                    this.man.setTexture('man_hit');
                    this.hitMan(1);
                }
                break;

            case this.STATE_GAME_PLAYINGP:
                this.pressEnable = true;
                this.man.setTexture('man_stand');
                break;


        }
    }
}
export default Playing;