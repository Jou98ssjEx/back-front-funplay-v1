class Menu extends Phaser.Scene {
    constructor() {
        super('Menu');
    }
    init(data) {
        this.points = 0;
        if (Object.keys(data).length !== 0) {
            this.points = data.points;
        }
    }

    preload() {
        console.log('Menu Principal');
    }

    create() {

        const pointsDB = localStorage.getItem('best_points_pin');
        this.betsPoints = (pointsDB !== null) ? pointsDB : 0;

        this.bg = this.add.sprite(0, 0, 'background');

        // this.objects1 = this.add.sprite(0, 0, 'objects1');
        // this.objects1.setOrigin(0);
        // this.objects2 = this.add.sprite(0, 0, 'objects2');
        // this.objects2.setOrigin(0);


        this.cp = this.add.image(160, 100, 'btntIni');
        this.cp.setInteractive({ useHandCursor: true });
        this.bg.setOrigin(0);

        this.btnCAM = this.add.image(900, 30, 'btnCAM').setInteractive({ useHandCursor: true });
        this.btnCEx = this.add.image(900, 400, 'btnCEx').setInteractive({ useHandCursor: true });

        this.jugador = this.add.image(120, 330, 'jugador');
        this.agCho = this.add.image(185, 332, 'agCho');
        this.agCho.setOrigin(0, 1);
        this.mano = this.physics.add.image(130, 355, 'mano1');
        this.mano.setOrigin(0, 0.5);
        this.mano.angle = 335;



        // var atlasTexture = this.textures.get('penguis');
        // console.log(atlasTexture);
        // var frames = atlasTexture.getFrameNames();
        // console.log(frames);

        // this.physics.
        this.pl = this.add.sprite(560, 190, 'penguis').setScale(0.8);
        this.pl.anims.load('pengs');
        this.pl.play('pengs');

        // for (var i = 0; i < frames.length; i++) {
        //     var x = Phaser.Math.Between(0, 800);
        //     var y = Phaser.Math.Between(0, 600);

        //     this.add.image(x, y, 'penguis', frames[i]);
        // }

        // this.add.image(560, 190, 'peng');
        // this.pe = this.add.image(660, 290, 'penguis');
        // this.pe
        // this.add.
        this.add.image(520, 200, 'explosion');

        // fullscreen
        const fullscreen = (e) => {
                if (e.webkitRequestFullScreen) {
                    e.webkitRequestFullScreen();
                } else if (e.mozRequestFullScreen) {
                    e.mozRequestFullScreen();
                }
            }
            // Botn para el fullScreen
        this.btnCAM.on('pointerdown', function(pointer) {
            this.setTint(0x4e9ea5);
        });
        this.btnCAM.on('pointerout', function(pointer) {
            this.clearTint();
        });
        this.btnCAM.on('pointerup', () => {
            fullscreen(document.getElementById('ingreso'));
        });

        this.btnCEx.on('pointerdown', function(pointer) {
            this.setTint(0xff3243);
        });
        this.btnCEx.on('pointerout', function(pointer) {
            this.clearTint();
        });
        this.btnCEx.on('pointerup', function(pointer) {
            location.href = '../start'
                // this.clearTint();
        });


        this.pointsText = this.add.bitmapText(
            this.scale.width - 390,
            this.scale.height - 70,
            'pixelFont',
            'PUNTOS ' + this.points
        ).setDepth(2).setOrigin(0.5);

        this.bestPointsText = this.add.bitmapText(
            this.scale.width - 390,
            this.scale.height - 40,
            'pixelFont',
            'MEJOR  ' + this.betsPoints
        ).setDepth(2).setOrigin(0.5);


        // local storage de los mejores puntos
        if (this.points > this.betsPoints) {
            localStorage.setItem('best_points_pin', this.points);
            // aqui puedes enviarlo a una db
        }

        this.cp.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.add.tween({
                targets: this.cp,
                ease: 'Bounce.easeIn',
                y: -200,
                duration: 1000,
                onComplete: () => {
                    // this.scene.start('Play');
                    this.scene.start('Playing');
                }
            });
        });
    }
    update(time, delta) {

        // this.pl.setVelocityX(200);
        // Movimientos de personajes secundarios
        // this.pl.x--;
        // if (this.pl.x < -300) {
        //     this.pl.x = 940;
        // }
        // var pointerX = this.input.x;
        // var pointerY = this.input.y;

        // console.log('x:' + pointerX);
        // console.log('y:' + pointerY);

        // var ang = Phaser.Math.Angle.Between(this.mano.x, this.mano.y, pointerX, pointerY + 12);
        // console.log(ang);

        // this.mano.rotation = ang;

        // this.pl.y += 0.3;

        // this.pl.x += 0.3;
        // this.pl.x++;
        // if (this.pl >= 940) {
        //     this.pl.x = 1;
        // }

        this.pl.x += 0.3;
        this.pl.y -= 0.1;

        if (this.pl.x >= 700) {
            this.pl.y += 0.7;
            if (this.pl.x >= 800) {
                this.pl.y -= 0.7;
            }
            if (this.pl.y >= 300) {
                this.pl.y -= 0.1;
                this.pl.x -= 100;
            }
            // if ()
        }
    }

}
export default Menu;