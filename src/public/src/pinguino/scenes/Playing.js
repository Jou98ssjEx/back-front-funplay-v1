class Playing extends Phaser.Scene {
    constructor() {
        super('Playing');
    }
    init() {

        this.CantidadP = 1;
        this.actual_points = 0;
        this.tiempoDeley = 30000;
        this.tiempoSegundos = 30;


    }
    preload() {
        console.log('Inicio del juego');
    }
    create() {
        this.bg = this.add.sprite(0, 0, 'background');
        this.bg.setOrigin(0);

        this.points = this.add.bitmapText(
            this.scale.width / 2 - 370,
            25,
            'pixelFont',
            'PUNTOS  ' + this.actual_points,
        ).setDepth(2).setOrigin(0.5);

        this.timerText = this.add.bitmapText(
            this.scale.width / 2,
            400,
            'pixelFont',
            'TIME ',
        ).setDepth(2).setOrigin(0);
        this.btnCAM = this.add.image(900, 30, 'btnCAM').setInteractive({ useHandCursor: true });


        this.jugador = this.add.image(120, 330, 'jugador');


        this.pl = this.physics.add.sprite(560, 190, 'penguis').setScale(0.8).setSize(40, 70);
        this.pl.anims.load('pengs');
        this.pl.play('pengs');
        this.pl.setInteractive({ useHandCursor: true });

        this.explo = this.physics.add.image(300, 300, 'explosion');
        this.explo.setOrigin(0.6);
        this.explo.visible = false;


        this.t = this.physics.add.sprite(130, 342, 'rayo');

        this.t.setScale(1, 0.7);
        this.t.visible = false;
        this.t.isDisparar = false;

        console.log('no dispara');

        // this.t.setOrigin(0, 1.4);
        this.t.anims.load('rays');
        this.t.play('rays');

        this.mano = this.physics.add.image(130, 355, 'mano1');
        this.mano.setOrigin(0, 0.5);


        this.pl.on(Phaser.Input.Events.POINTER_DOWN, () => {

        });
        this.timerC = this.time.addEvent({
            // delay: 20000, // ms
            delay: this.tiempoDeley, // ms
            callback: () => {
                if (this.timerC.getOverallProgress() == 1) {
                    this.timerC.remove();
                    console.log('Fin del Juego');
                    // alert('cero');
                    this.scene.start('Menu', { points: this.actual_points });

                }
            },
            //args: [],
            callbackScope: this,
            // paused: true
            // loop: false
        });

        // fullscreen
        const fullscreen = (e) => {
                if (e.webkitRequestFullScreen) {
                    e.webkitRequestFullScreen();
                } else if (e.mozRequestFullScreen) {
                    e.mozRequestFullScreen();
                }
            }
            // Botn para el fullScreen
        this.btnCAM.on('pointerdown', function(pointer) {
            this.setTint(0x4e9ea5);
        });
        this.btnCAM.on('pointerout', function(pointer) {
            this.clearTint();
        });
        this.btnCAM.on('pointerup', () => {
            fullscreen(document.getElementById('ingreso'));
        });

        this.Anin1();

    }

    increaseScore() {
        this.actual_points += this.CantidadP;
        this.points.text = `PUNTOS ${this.actual_points}`;
    }



    rayoS(XStr, yStr, xF, yF) {


        this.t.xStart = XStr;
        this.t.yStart = yStr;
        this.t.xFinal = xF;
        this.t.yFinal = yF;

        this.t.y = this.t.yStart;
        this.t.x = this.t.xStart;


        var angl = Phaser.Math.Angle.Between(this.t.xFinal, this.t.yFinal, this.t.xStart, this.t.yStart);

        this.t.rotation = angl + Math.PI;
        this.t.setOrigin(0, 1.4);

        var deltax = this.t.xFinal - this.t.xStart;
        var deltay = this.t.yFinal - this.t.yStart;
        var widthRay = Math.sqrt(deltax * deltax + deltay * deltay);


        this.RayoTweens = this.tweens.add({
            targets: this.t,
            paused: true,
            ease: 'Cubic',
            duration: 1500,
            displayWidth: widthRay,
            onComplete: () => {
                // console.log('e: ' + this.pl.getBounds().left);
                // this.t.visible = false;


                if (this.t.xFinal > this.pl.getBounds().left && this.t.xFinal < this.pl.getBounds().right && this.t.yFinal > this.pl.getBounds().top && this.t.yFinal < this.pl.getBounds().bottom) {
                    this.explos();
                    // this.t.isDisparar = false;

                } else {
                    this.t.visible = false;
                    // this.t.isDisparar = false;

                }

            }
        });

    }

    onTap() {


        var pointerX = this.input.x;
        var pointerY = this.input.y;

        var ang = Phaser.Math.Angle.Between(pointerX, pointerY, this.mano.x, this.mano.y);
        var point = new Phaser.Math.Vector2(pointerX - this.mano.x, pointerY - this.mano.y);
        // console.log(point);
        let abc = point.normalize();
        // console.log(abc);

        if (this.input.activePointer.isDown == true) {

            this.t.visible = true;
            this.t.displayWidth = 0;
            this.RayoTweens.resume();
            this.sound.play('sfxHit', { loop: false });

        }

        this.rayoS(this.mano.x + abc.x * 70, this.mano.y + abc.y * 70, pointerX, pointerY);
        this.mano.rotation = ang + Math.PI;

    }
    explos() {
        this.explo.visible = true;
        this.explo.x = this.t.xFinal;
        this.explo.y = this.t.yFinal;
        this.explo.setScale(0.5);

        this.tweens.add({
            targets: this.explo,
            scale: 1,
            onStart: () => {
                this.sound.play('sfxLoose', { loop: false });

            },
            // scale: { from: 1, to: 1 },
            duration: 1300,
            ease: 'Cubic',
            onComplete: () => {
                this.increaseScore();

                this.t.visible = false;

                this.explo.visible = false;

            }

        });
        console.log('clickasdda');

    }
    Anin1() {

        this.timeTw = this.tweens.timeline({
            targets: this.pl,
            duration: 2800,
            ease: 'Cubic.easeInOut',
            loop: -1,
            tweens: [{
                    y: 130,
                    x: 180
                },
                {
                    y: 350,
                    x: 300
                },
                {
                    y: 220,
                    x: 600
                },
                {
                    y: 100,
                    x: 800
                },
                {
                    y: 310,
                    x: 870
                },
                {
                    x: this.scale.width / 2,
                    y: this.scale.height / 2
                },
                {
                    x: 320,
                    y: 320
                },
                {
                    y: 40,
                },
                {
                    x: 560,
                    y: 190
                }
            ]
        });

    }

    update(time, delta) {

        console.log('update: ' + this.t.isDisparar);
        if (this.t.isDisparar == false) {
            this.onTap();
        }


        this.timerText.setText(Math.floor(this.tiempoSegundos - this.timerC.getElapsedSeconds()));

    }
}
export default Playing;