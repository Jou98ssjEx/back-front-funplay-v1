var STATE_GAME_NONE = 0;
var STATE_GAME_LOADING = 1;
var STATE_GAME_LOADED = 2;
var STATE_GAME_PLAYING = 3;
var STATE_GAME_LEVEL_COMPLETE = 4;
var STATE_GAME_GAMEOVER = 5;

var stateGame = STATE_GAME_NONE;

GamePlayManager = {
    init: function() {
        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;
    },
    preload: function() {
        stateGame = STATE_GAME_LOADING;

        game.load.atlasJSONArray('assets_game', 'assets/atlas/assets_game.png', 'assets/atlas/assets_game.json');

        game.load.spritesheet('buttonPlay', 'assets/images/buttonPlay.png', 200, 76, 2);

        //SOUNDS
        game.load.audio('loopMusic', 'assets/sounds/musicLoop.mp3');
        game.load.audio('sfxShoot', 'assets/sounds/sfxShoot.mp3');
        game.load.audio('sfxHit', 'assets/sounds/sfxHit.mp3');
    },
    create: function() {
        this.highscore = localStorage.getItem('highscore');
        if (this.highscore == null) {
            this.highscore = 0;
        } else {
            this.highscore = parseInt(this.highscore);
        }

        game.input.onTap.add(this.onTap, this);
        //BG
        game.add.sprite(0, 0, 'assets_game', 'background0000');
        //KUM
        this.kumBody = game.add.sprite(175, 670, 'assets_game', 'KumBody0000');
        this.kumBody.anchor.setTo(0.5, 1);
        this.kumHand = game.add.sprite(192, 613, 'assets_game', 'KumHand0000');
        this.kumHand.anchor.setTo(0, 0.5);
        //PENGUIN
        this.penguin = game.add.sprite(551, 400, 'assets_game', 'Penguin0000');
        this.penguin.anchor.setTo(0.5, 1);
        this.penguin.animations.add('loop', ['Penguin0000', 'Penguin0001', 'Penguin0002', 'Penguin0003', 'Penguin0004', 'Penguin0003', 'Penguin0002', 'Penguin0001']);
        //RAY
        this.ray = new Ray();
        game.add.existing(this.ray);

        //FINAL EFFECT
        this.finalEffect = game.add.sprite(100, 100, 'assets_game', 'rayFinal0000');
        this.finalEffect.anchor.setTo(0.5);
        this.finalEffect.visible = false;

        //STYLE TEXT
        var style = {
            font: 'bold 20pt Arial',
            fill: '#03699E',
            align: 'center'
        }

        //HIGSCORE
        this.txtHighScore = game.add.text(game.width / 2, 35, "", style);
        this.txtHighScore.anchor.setTo(0.5);
        this.refreshTxtHighscore();

        //TIME LEFT TEXT
        this.txtTimeLeft = game.add.text(900, 40, "", style);
        this.txtTimeLeft.anchor.setTo(0.5);

        //SCORE TEXT
        this.currentScore = 0;
        this.txtCurrentScore = game.add.text(40, 35, "", style);
        this.txtCurrentScore.anchor.setTo(0, 0.5);
        this.refreshScore();

        //Bg Black
        var pixel = game.add.bitmapData(1, 1);
        pixel.ctx.fillStyle = '#000000';
        pixel.ctx.fillRect(0, 0, 1, 1);

        this.bgMenu = game.add.sprite(0, 0, pixel);
        this.bgMenu.anchor.setTo(0, 0);
        this.bgMenu.width = game.width;
        this.bgMenu.height = game.height;
        this.bgMenu.alpha = 0.5;

        //Button Play
        this.buttonPlay = game.add.button(game.width / 2, game.height * 0.8, 'buttonPlay', this.startGame, this, 1, 0, 1, 0);
        this.buttonPlay.anchor.setTo(0.5);

        //SOUNDS
        this.loopMusic = game.add.audio('loopMusic');
        this.sfxShoot = game.add.audio('sfxShoot');
        this.sfxHit = game.add.audio('sfxHit');
    },
    refreshTxtHighscore: function() {
        this.txtHighScore.text = "highscore: " + this.highscore;
    },
    refreshScore: function() {
        this.txtCurrentScore.text = "score: " + this.currentScore.toString();
    },
    increaseScore: function() {
        this.sfxHit.play();

        this.currentScore += 100;
        this.refreshScore();
    },
    startGame: function() {
        stateGame = STATE_GAME_PLAYING;

        this.loopMusic.loop = true;
        this.loopMusic.play();

        this.currentScore = 0;
        this.refreshScore();

        this.timeLeft = 10;
        this.txtTimeLeft.text = this.timeLeft;

        this.timerCountDown = game.time.events.loop(Phaser.Timer.SECOND * 1, this.callbackTimeDown, this);

        this.buttonPlay.visible = false;
        this.bgMenu.visible = false;

        this.penguin.animations.play('loop', 48, true);

        //PENGUIN
        this.penguin.index = 0;
        this.penguin.path = [];
        this.penguin.points = {
            'x': [400, 600, 731, 950, 862, 926, 700, 550, 400],
            'y': [400, 300, 270, 300, 400, 350, 500, 550, 400]
        };

        var x = 1 / game.width;

        for (var i = 0; i <= 1; i += x) {
            var px = game.math.catmullRomInterpolation(this.penguin.points.x, i);
            var py = game.math.catmullRomInterpolation(this.penguin.points.y, i);

            this.penguin.path.push({ x: px, y: py });
        }
        console.log("LENGHT:" + this.penguin.path.length);

    },
    callbackTimeDown: function() {
        this.timeLeft--;
        if (this.timeLeft <= 0) {
            this.timeLeft = 0;
            this.levelComplete();
            game.time.events.remove(this.timerCountDown);
        }
        this.txtTimeLeft.text = this.timeLeft;
    },
    levelComplete: function() {
        stateGame = STATE_GAME_LEVEL_COMPLETE;
        this.loopMusic.stop();

        this.penguin.animations.stop();
        this.ray.GameOver();
        this.finalEffect.visible = false;

        if (this.currentScore > this.highscore) {
            localStorage.setItem('highscore', this.currentScore);
            this.highscore = this.currentScore;
            this.refreshTxtHighscore();
        }
        this.bgMenu.visible = true;
        this.buttonPlay.visible = true;
    },
    update: function() {
        if (stateGame == STATE_GAME_PLAYING) {
            this.penguin.x = this.penguin.path[this.penguin.index].x;
            this.penguin.y = this.penguin.path[this.penguin.index].y;

            this.penguin.index++;

            if (this.penguin.index >= this.penguin.path.length) {
                this.penguin.index = 0;
            }
        }

    },
    onTap: function(pointer) {
        if (stateGame == STATE_GAME_PLAYING) {
            if (!this.ray.isShooting) {
                var xMouse = pointer.x;
                var yMouse = pointer.y;
                var angle = game.math.angleBetween(xMouse, yMouse, this.kumHand.x, this.kumHand.y);
                this.kumHand.rotation = angle + Math.PI;

                var direction = new Phaser.Point(xMouse - this.kumHand.x, yMouse - this.kumHand.y);
                direction.normalize();
                this.ray.Shoot(this.kumHand.x + direction.x * 80, this.kumHand.y + direction.y * 80, xMouse, yMouse, this.penguin, this.finalEffect, this.sfxShoot);
            }
        }
    },
    render: function() {
        //game.debug.spriteBounds(this.penguin);
        //game.debug.spriteBounds(this.kumBody);
    }
}

var game = new Phaser.Game(940, 440, Phaser.AUTO);

game.state.add("gameplay", GamePlayManager);
game.state.start("gameplay");