class Bootloader extends Phaser.Scene {
    constructor() {
        super('Bootloader');
    }

    preload() {
        // Ruta de las imagenes
        this.load.path = '/assets/play6/assets/';

        this.load.image([
            'background',
            'jugador',
            'mano',
            'mano1',
            'agCho',
            'peng',
            'explosion',
            'btntIni',
            'btnCAM',
            'btnCEx',
        ]);


        this.load.image('font', '../font/font.png');
        this.load.json('fontData', '../font/font.json');

        this.load.audio('loopMusic', '../sounds/musicLoop.mp3');
        this.load.audio('sfxHit', '../sounds/sfxHit.mp3');
        this.load.audio('sfxLoose', '../sounds/sfxShoot.mp3');

        this.load.atlas('penguis', 'penguis.png', 'penguis_atlas.json');
        this.load.animation('penguisAnim', 'penguis_anim.json');

        this.load.atlas('rayo', 'rayo.png', 'rayo_atlas.json');
        this.load.animation('rayoAnim', 'rayo_anim.json');

        this.load.on('complete', () => {

            this.sound.play('loopMusic', { loop: true });

            const fontData = this.cache.json.get('fontData');
            this.cache.bitmapFont.add('pixelFont', Phaser.GameObjects.RetroFont.Parse(this, fontData));

            // this.scene.start('Playing');
            this.scene.start('Menu');

        });

    }




}
export default Bootloader;