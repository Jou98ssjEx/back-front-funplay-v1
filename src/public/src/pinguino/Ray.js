Ray = function() 
{
    Phaser.Sprite.call(this, game, 200, 200, 'assets_game', 'ray0000');
    this.animations.add('shoot', ['ray0000', 'ray0001', 'ray0002', 'ray0003']);
    this.animations.play('shoot', 24, true);
    
    this.anchor.setTo(0, 1.2);
    this.visible = false;
    
    this.isShooting = false;
};

Ray.prototype = Object.create(Phaser.Sprite.prototype);
Ray.prototype.constructor = Ray;

Ray.prototype.GameOver = function() {
    if(this.tweenRay!=null){
        this.tweenRay.stop();
    }
    if(this.tweenFinalEffect!=null){
        this.tweenFinalEffect.stop();
    }
    this.visible = false;
}

Ray.prototype.Shoot = function(xStart, yStart, xFinal, yFinal, penguin, finalEffect, sfxShoot) {
      
    if(this.isShooting){
        return;
    }
    
    sfxShoot.play();
    
    finalEffect.visible = false;
    this.isShooting = true;
    this.xStart = xStart;
    this.yStart = yStart;
    this.xFinal = xFinal;
    this.yFinal = yFinal;
    
    this.x = this.xStart;
    this.y = this.yStart;
    
    this.visible = true;
    
    var angle = game.math.angleBetween(this.xFinal, this.yFinal,this.xStart, this.yStart);
    this.rotation = angle + Math.PI;
    
    var deltax = this.xFinal - this.xStart;
    var deltay = this.yFinal - this.yStart;
    var widthRay = Math.sqrt(deltax*deltax + deltay*deltay);
    
    this.width = 20;
    
    var startAnimation = widthRay * 2;
    this.tweenRay = game.add.tween(this).to({
        width: [widthRay]
    }, startAnimation, Phaser.Easing.Cubic.Out, false, 0, 0, false).start();
    
    this.tweenRay.onComplete.add(function(){
        this.visible = false;
        
        if(this.xFinal > penguin.left && this.xFinal < penguin.right && this.yFinal > penguin.top && this.yFinal < penguin.bottom){
            GamePlayManager.increaseScore();
            
            finalEffect.visible = true;
            finalEffect.x = this.xFinal;
            finalEffect.y = this.yFinal;
            finalEffect.scale.setTo(0.5);
            
            this.tweenFinalEffect = game.add.tween(finalEffect.scale).to({x:1, y:1}, 300, Phaser.Easing.Cubic.Out);
            
            this.tweenFinalEffect.onComplete.add(function(){
                this.isShooting = false;
                finalEffect.visible = false;
            },this);
            this.tweenFinalEffect.start();
        }else{
            this.isShooting = false;
        }
    },this);
}



