const config = {
    width: 900,
    height: 0,
    parent: "ingreso",
    type: Phaser.AUTO,
    scene: {
        preload,
        create
    },
    scale: {
        mode: Phaser.ScaleModes.SHOW_ALL
    }
}

const game = new Phaser.Game(config);

function preload() {

    // Ruta de las imagenes
    this.load.path = 'assets/play1/sounds/';

    this.load.audio('pum', 'song.mp3');

}

function create() {
    this.s = this.sound.add("pum", { loop: true });
    this.s.play();
}