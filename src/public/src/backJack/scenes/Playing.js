class Playing extends Phaser.Scene {
    constructor() {
        super('Playing');
    }
    init() {

    }
    preload() {
        console.log('Inicio del juego');

        // Ruta de las imagenes
        this.load.path = '/assets/play2/assets/';

        const deck = [];
        const tipos = ['C', 'D', 'H', 'S'];
        const especiales = ['A', 'J', 'Q', 'K'];

        for (let i = 2; i <= 10; i++) {
            for (let tipo of tipos) {
                deck.push(i + tipo);
                let a = this.load.image(`${i}${tipo}`, `${i}${tipo}.png`);
                console.log('carta:' + `${i}${tipo}`);
            }
        }

        for (let tipo of tipos) {
            for (let esp of especiales) {
                deck.push(esp + tipo);
                let b = this.load.image(`${esp +tipo}`, `${esp +tipo}.png`);
                console.log('cartaS:' + `${esp +tipo}`);
            }
        }
        console.log(deck);

        this.d = deck

    }
    create() {
        // Mostrar Background
        this.back = this.physics.add.image(0, 0, "bg");
        this.back.setOrigin(0, 0);

        // this.deck = this.add.image(100, 100, `haifosdf`);

        this.btn3 = this.add.image(880, 410, "btn3").setInteractive({ useHandCursor: true });

        // Cambiar de escena
        this.btn3.on('pointerup', () => {
            this.scene.start('Menu', 'fin');
        });
    }


    update(time, delta) {

    }
}
export default Playing;