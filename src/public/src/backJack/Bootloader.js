class Bootloader extends Phaser.Scene {
    constructor() {
        super('Bootloader');
    }

    preload() {
        // Ruta de las imagenes
        this.load.path = '/assets/play2/assets/';

        // Background
        this.load.image('bg', 'FonBJ.png');

        // Personajes Principal
        this.load.image('logo', 'logoBJ.png');
        this.load.image('cPlay', 'CatasEmpezar.png');
        this.load.image('cExit', 'cartas-exit.png');



        // // Botones del menu
        this.load.image('btn_play', 'btn-play.png');
        // this.load.image('btn_info', 'btn-Info.png');
        this.load.image('btn_exit', 'btn-exit.png');

        // this.load.image('boton_Jugar', 'toB.png');
        // this.load.image('boton_Salir', 'toB2.png');

        // // Botones circulares
        // this.load.image('btn', 'btn.png');
        // this.load.image('btn1', 'btn1.png');
        // this.load.image('btn2', 'btn2.png');
        this.load.image('btn3', 'btn3.png');

        // // Titulo del juego
        // this.load.image('non', 'Titulo.png');

        // // Cargar el sprite
        // this.load.atlas('diamantes', 'diamantes.png', 'diamantes_atlas.json');

        // // Img de exposion
        // this.load.image('explosion', 'explosion.png');

        // // Tabla de datos
        // this.load.image('tabla', 'tabla.png');
        // this.load.image('tabla1', 'tabla1.png');

        // // // Cargando el audio de Load
        // // this.load.audio('intro', 'assets/play1/sounds/musicLoop.mp3');
        // this.load.audio('pum', '../sounds/Pop.mp3');


        // console.log( deck );
        // deck = _.shuffle(deck);
        // return deck;


        // this.load.image(deck, `${deck}.png`);
        // console.log(this.cartas);


        this.scene.start('Load');
    }




}
export default Bootloader;