class Bootloader extends Phaser.Scene {
    constructor() {
        super('Bootloader');
    }

    preload() {
        // Ruta de las imagenes
        this.load.path = '/assets/play1/assets/';

        // Background
        this.load.image('bg', 'fondo.png');

        // Personajes Principal
        this.load.image('caballo', 'heroe.png');
        this.load.image('ca', 'heroe1.png');

        // Personajes Secundarios
        this.load.image('shark', 'shark.png');
        this.load.image('fishes', 'fishes.png');
        this.load.image('mollusk', 'mollusk.png');
        this.load.image('booble1', 'booble1.png');
        this.load.image('booble2', 'booble2.png');

        // Botones del menu
        this.load.image('btn_play', 'btn-Play1.png');
        this.load.image('btn_info', 'btn-Info.png');
        this.load.image('btn_exit', 'btn-Exit.png');

        this.load.image('boton_Jugar', 'toB.png');
        this.load.image('boton_Salir', 'toB2.png');

        // Botones circulares
        this.load.image('btn', 'btn.png');
        this.load.image('btn1', 'btn1.png');
        this.load.image('btn2', 'btn2.png');
        this.load.image('btn3', 'btn3.png');

        // Titulo del juego
        this.load.image('non', 'Titulo.png');

        // Cargar el sprite
        this.load.atlas('diamantes', 'diamantes.png', 'diamantes_atlas.json');

        // Img de exposion
        this.load.image('explosion', 'explosion.png');

        // Tabla de datos
        this.load.image('tabla', 'tabla.png');
        this.load.image('tabla1', 'tabla1.png');

        // // Cargando el audio de Load
        // this.load.audio('intro', 'assets/play1/sounds/musicLoop.mp3');
        this.load.audio('pum', '../sounds/Pop.mp3');

        this.scene.start('Load');

    }




}
export default Bootloader;