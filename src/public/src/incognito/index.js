import BootLoader from '/src/incognito/Bootloader.js';
import Load from '/src/incognito/scenes/Load.js';
import Menu from '/src/incognito/scenes/Menu.js';
import Playing from '/src/incognito/scenes/Playing.js';

// import Playing from '/src/scenes/Playing.js';


const lienzo = document.getElementById('ingreso');
// 1295
// 792

const config = {
    width: 940,
    height: 440,
    parent: "ingreso",
    // background: ruta,
    type: Phaser.AUTO,
    // pixelArt: true,
    scene: [
        BootLoader,
        Load,
        Menu,
        Playing,

    ],
    physics: {
        default: "arcade",
        arcade: {
            gravity: {
                // y: 500
            },
            // debug: true
        }
    },
    scale: {
        mode: Phaser.ScaleModes.SHOW_ALL
    }
}

const game = new Phaser.Game(config);
export default game;