class Menu extends Phaser.Scene {
    constructor() {
        super('Menu');

    }

    preload() {
        console.log('Menu Principal');
    }



    create() {

        this.back = this.physics.add.image(0, 0, "bg");
        this.back.setOrigin(0, 0);
        this.caballo = this.physics.add.image(550, -240, "caballo");

        this.mollusk = this.add.image(680, 250, 'mollusk');
        this.mollusk1 = this.add.image(500, 400, 'mollusk');
        this.mollusk2 = this.add.image(170, 240, 'mollusk');
        this.shark = this.add.image(200, 30, 'shark');
        this.shark2 = this.add.image(800, 100, 'shark');
        this.fishes = this.add.image(360, 380, 'fishes');

        // Interacion de boton PLay
        this.btn_play = this.physics.add.image(-250, -140, "btn_play", this.starGame, this);
        this.btn_play.setInteractive({ useHandCursor: true });

        this.btn_play.on('pointerdown', function(pointer) {
            this.setTint(0xff0000);
        });
        // Cuando el usuario se desplaza x el boton
        // this.btn_play.on('pointerover', () => {
        // });

        // Cuando el usuario abandona el boton
        this.btn_play.on('pointerout', function(pointer) {
            this.clearTint();
        });

        // Con esta funcion cambias la escena a traves del evento del boton ---Actualiza
        this.btn_play.on('pointerup', () => {
            this.scene.start('Playing', 'fin');
        });


        // INfo
        this.btn_info = this.physics.add.image(-300, -200, "btn_info");
        this.btn_info.setInteractive({ useHandCursor: true });

        this.btn_info.on('pointerdown', function(pointer) {
            this.setTint(0xff3243);
        });
        this.btn_info.on('pointerout', function(pointer) {
            this.clearTint();
        });
        this.btn_info.on('pointerup', () => {
            this.scene.start('Intro', 'fin');
        });

        // Exit btn
        this.btn_exit = this.physics.add.image(-340, -270, "btn_exit");
        this.btn_exit.setInteractive({ useHandCursor: true });

        this.btn_exit.on('pointerdown', function(pointer) {
            this.setTint(0xff3243);
        });
        this.btn_exit.on('pointerout', function(pointer) {
            this.clearTint();
        });
        this.btn_exit.on('pointerup', function(pointer) {
            location.href = '../start'
                // this.clearTint();
        });

        // Botones circulares
        this.btn = this.physics.add.image(-40, -410, "btn").setInteractive({ useHandCursor: true });
        this.btn1 = this.physics.add.image(-100, -410, "btn1").setInteractive({ useHandCursor: true });
        this.btn2 = this.physics.add.image(-160, -410, "btn2").setInteractive({ useHandCursor: true });
        this.btn3 = this.physics.add.image(-880, -410, "btn3").setInteractive({ useHandCursor: true });

        this.btn.on('pointerdown', function(pointer) {
            this.setTint(0xff3243);
        });
        this.btn.on('pointerout', function(pointer) {
            this.clearTint();
        });
        this.btn.on('pointerup', function(pointer) {
            var box = document.createElement('div');
            box.style.cssText = 'border:8px solid #56aaf3;padding:12px;width:160px;margin:12px 0 12px 0; background: red';
            // var box = document.createElement('input');
            // box.style.background = 'red';
        })

        // fullscreen
        const fullscreen = (e) => {
                if (e.webkitRequestFullScreen) {
                    e.webkitRequestFullScreen();
                } else if (e.mozRequestFullScreen) {
                    e.mozRequestFullScreen();
                }
            }
            // Botn para el fullScreen
        this.btn3.on('pointerup', () => {
            fullscreen(document.getElementById('ingreso'));
        });

        // Titulo
        this.non = this.physics.add.image(-650, -100, "non");
        this.non.setScale(1.3);

        // Animacion del titulo
        let tween_titulo = this.tweens.add({
            targets: [this.non],
            props: {
                x: {
                    value: 650,
                    duration: 1000
                },
                y: {
                    value: 100,
                    duration: 1000
                }
            },
            delay: 1000
        });
        // Aminacion del personaje
        let tween_caballo = this.tweens.add({
            targets: [this.caballo],
            props: {
                x: {
                    value: 550,
                    duration: 1000
                },
                y: {
                    value: 240,
                    duration: 1000
                }
            },
            delay: 1000
        });
        // Animación de los botones del menu
        let tween_btn_play = this.tweens.add({
            targets: [this.btn_play],
            props: {
                x: {
                    value: 250,
                    duration: 1000
                },
                y: {
                    value: 140,
                    duration: 1000
                }
            },
            delay: 1000
        });
        let tween_btn_info = this.tweens.add({
            targets: [this.btn_info],
            props: {
                x: {
                    value: 300,
                    duration: 1000
                },
                y: {
                    value: 200,
                    duration: 1000
                }
            },
            delay: 1000
        });
        let tween_btn_exit = this.tweens.add({
            targets: [this.btn_exit],
            props: {
                x: {
                    value: 340,
                    duration: 1000
                },
                y: {
                    value: 270,
                    duration: 1000
                }
            },
            delay: 1000
        });

        // Animación de botones secundarios
        let tween_btn = this.tweens.add({
            targets: [this.btn],
            props: {
                x: {
                    value: 40,
                    duration: 1000
                },
                y: {
                    value: 410,
                    duration: 1000
                }
            },
            delay: 1000
        });
        let tween_btn1 = this.tweens.add({
            targets: [this.btn1],
            props: {
                x: {
                    value: 100,
                    duration: 1000
                },
                y: {
                    value: 410,
                    duration: 1000
                }
            },
            delay: 1000
        });
        let tween_btn2 = this.tweens.add({
            targets: [this.btn2],
            props: {
                x: {
                    value: 160,
                    duration: 1000
                },
                y: {
                    value: 410,
                    duration: 1000
                }
            },
            delay: 1000
        });
        let tween_btn3 = this.tweens.add({
            targets: [this.btn3],
            props: {
                x: {
                    value: 880,
                    duration: 1000
                },
                y: {
                    value: 410,
                    duration: 1000
                }
            },
            delay: 1000
        });


        // Selecion dentro y fuera del canvas
        var graphics = this.add.graphics();

        graphics.fillStyle(0x000000, 0.5);
        graphics.fillRect(0, 0, 940, 440);
        graphics.setVisible(false);

        this.input.on('gameout', function() {
            graphics.setVisible(true);
        });
        this.input.on('gameover', function() {
            graphics.setVisible(false);
        });

    }




    update(time, delta) {

    }

}
export default Menu;