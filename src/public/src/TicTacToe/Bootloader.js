class Bootloader extends Phaser.Scene {
    constructor() {
        super('Bootloader');
    }

    init() {
        console.log('Scene Bootloader');
    }
    preload() {

        // Ruta de las imagenes
        this.load.path = '/assets/play2/assets/';

        // this.load.setPath('./assets/');
        this.load.image([
            'bg',
            'btnE',
            'btnS',
            'btnIni',
            'lbExpan',
            'lbSalir',
            'cero_opaco',
            'cero',
            'equis',
            'equis_opaco',
            'position',
            'reload',
            'tablero_win',
            'tablero'
        ]);
        this.load.image('font', '../font/font.png');
        this.load.json('fontConfig', '../font/font.json');

        this.load.audio('draw', '../sounds/draw.mp3');
        this.load.audio('pop', '../sounds/pop.mp3');
        this.load.audio('win', '../sounds/win.mp3');
        this.load.audio('matrix', '../sounds/matrix.mp3');

        this.load.on('complete', () => {
            const fontConfig = this.cache.json.get('fontConfig');
            this.cache.bitmapFont.add('pixelFont', Phaser.GameObjects.RetroFont.Parse(this, fontConfig));
            // this.s = this.sound.add("matrix", { loop: false });
            // this.s.play();
            this.scene.start('Menu');
        });
    }



}
export default Bootloader;