class Playing extends Phaser.Scene {
    constructor() {
        super('Playing');
    }
    init() {
        this.ID = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, ];
        // this.ID = [0, 1, 2, 3];
        this.X0 = [505, 746, 314, 749, 860, 320, 193, 68, 655, 8, 313];
        this.Y0 = [250, -50, 175, 235, 276, 190, 192, 260, 330, 140, 80];
        this.X1 = [505, 746, 410, 727, 860, 350, 146, 77, 637, 35, 414];
        this.Y1 = [170, 60, 120, 287, 199, 260, 240, 231, 300, 132, 56];
        this.scala = [0.7, 1, 1, 0.6, 0.7, 1, 1, 0.3, 0.35, 0.45, 1];
        this.angulo = [0, 180, 45, 195, 0, 135, 200, 17, -45, 62, 21];
        this.timeAnimate = [1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500];
        this.deleyAnimate = [1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500];

        this.ArrayJu = [];

        this.actual_points = 0;

        this.tiempoDeley = 30000;
        this.tiempoSegundos = 30;

        this.CantidadP = 5;

    }
    preload() {
        console.log('Inicio del juego');
    }
    create() {
        this.bg = this.add.sprite(0, 0, 'background');
        this.bg.setOrigin(0);
        //

        this.primerNin();

        this.time.addEvent({
            delay: 2000,
            callback: () => {
                // this.primerNin();
                this.callBackShowNinja();
            }
        });


        this.points = this.add.bitmapText(
            this.scale.width / 2 - 370,
            25,
            'pixelFont',
            'PUNTOS  ' + this.actual_points,
        ).setDepth(2).setOrigin(0.5);

        this.timerText = this.add.bitmapText(
            this.scale.width / 2,
            400,
            'pixelFont',
            'TIME ',
        ).setDepth(2).setOrigin(0);
        this.btnCAM = this.add.image(900, 400, 'btnCAM').setInteractive({ useHandCursor: true });

        this.timerC = this.time.addEvent({
            // delay: 20000, // ms
            delay: this.tiempoDeley, // ms
            callback: () => {
                if (this.timerC.getOverallProgress() == 1) {
                    this.timerC.remove();
                    console.log('Fin del Juego');
                    // alert('cero');
                    this.scene.start('Menu', { points: this.actual_points });

                }
            },
            //args: [],
            callbackScope: this,
            // paused: true
            // loop: false
        });

        // fullscreen
        const fullscreen = (e) => {
                if (e.webkitRequestFullScreen) {
                    e.webkitRequestFullScreen();
                } else if (e.mozRequestFullScreen) {
                    e.mozRequestFullScreen();
                }
            }
            // Botn para el fullScreen
        this.btnCAM.on('pointerdown', function(pointer) {
            this.setTint(0x4e9ea5);
        });
        this.btnCAM.on('pointerout', function(pointer) {
            this.clearTint();
        });
        this.btnCAM.on('pointerup', () => {
            fullscreen(document.getElementById('ingreso'));
        });
    }

    increaseScore() {
        this.actual_points += this.CantidadP;
        this.points.text = `PUNTOS ${this.actual_points}`;
    }


    primerNin() {
        for (let a = 0; a < 1; a++) {

            let caja = [];

            let posicion = Phaser.Math.Between(0, 10);
            let jugador = this.physics.add.image(this.X0[posicion], this.Y0[posicion], 'ninja');
            jugador.setScale(this.scala[posicion]);
            jugador.setAngle(this.angulo[posicion]);

            jugador.setInteractive({ useHandCursor: true });
            jugador.on(Phaser.Input.Events.POINTER_DOWN, () => {
                console.log('click');
                this.p = jugador.setTexture('smoks');
                // console.log(this.p);
                this.sound.play('sfxHit');

                this.increaseScore();
            });

            // jugador.on()

            this.objects1 = this.add.sprite(0, 0, 'objects1');
            this.objects1.setOrigin(0);
            this.objects2 = this.add.sprite(0, 0, 'objects2');
            this.objects2.setOrigin(0);
            // this.add.text(100, 100, 'Puntos:');

            this.TWJUGADOR = this.add.tween({
                targets: jugador,
                x: this.X1[posicion],
                y: this.Y1[posicion],
                duration: this.timeAnimate[posicion],
                ease: 'Quadratic.In',
                delay: this.deleyAnimate[posicion],
                yoyo: true,
                onComplete: () => {
                    // this.sound.play('sfxLoose');
                    jugador.destroy();
                    // this.time.addEvent({
                    //     delay: 1500
                    // })
                }
            });

            // this.sound.play('sfxLoose');


        }
    }


    // juegojugador
    preparateLevel() {
        this.time.addEvent({
            delay: 3000, // ms
            callback: () => {
                this.callBackShowNinja();
                // this.showNinja.destroy();
            },
            callbackScope: this,
        });
    }

    callBackShowNinja() {
        this.time.addEvent({
            delay: 3000, // ms
            callback: () => {
                this.callBackShowNinja();
            },
            callbackScope: this,
        });
        this.primerNin();
    }

    // Funciones del personaje


    update(time, delta) {


        this.timerText.setText(Math.floor(this.tiempoSegundos - this.timerC.getElapsedSeconds()));

    }
}
export default Playing;