class Menu extends Phaser.Scene {
    constructor() {
        super('Menu');
    }
    init(data) {
        this.points = 0;
        if (Object.keys(data).length !== 0) {
            this.points = data.points;
        }
    }

    preload() {
        console.log('Menu Principal');
    }

    create() {

        const pointsDB = localStorage.getItem('best_points_ninja');
        this.betsPoints = (pointsDB !== null) ? pointsDB : 0;

        this.bg = this.add.sprite(0, 0, 'background');

        this.objects1 = this.add.sprite(0, 0, 'objects1');
        this.objects1.setOrigin(0);
        this.objects2 = this.add.sprite(0, 0, 'objects2');
        this.objects2.setOrigin(0);


        this.cp = this.add.image(this.scale.width / 2, this.scale.height / 2 + 140, 'btntIni');
        this.cp.setInteractive({ useHandCursor: true })
        this.bg.setOrigin(0);

        this.btnCAM = this.add.image(900, 30, 'btnCAM').setInteractive({ useHandCursor: true });
        this.btnCEx = this.add.image(40, 30, 'btnCEx').setInteractive({ useHandCursor: true });

        // fullscreen
        const fullscreen = (e) => {
                if (e.webkitRequestFullScreen) {
                    e.webkitRequestFullScreen();
                } else if (e.mozRequestFullScreen) {
                    e.mozRequestFullScreen();
                }
            }
            // Botn para el fullScreen
        this.btnCAM.on('pointerdown', function(pointer) {
            this.setTint(0x4e9ea5);
        });
        this.btnCAM.on('pointerout', function(pointer) {
            this.clearTint();
        });
        this.btnCAM.on('pointerup', () => {
            fullscreen(document.getElementById('ingreso'));
        });

        this.btnCEx.on('pointerdown', function(pointer) {
            this.setTint(0xff3243);
        });
        this.btnCEx.on('pointerout', function(pointer) {
            this.clearTint();
        });
        this.btnCEx.on('pointerup', function(pointer) {
            location.href = '../start'
                // this.clearTint();
        });


        this.pointsText = this.add.bitmapText(
            this.scale.width - 90,
            this.scale.height - 70,
            'pixelFont',
            'PUNTOS ' + this.points
        ).setDepth(2).setOrigin(0.5);

        this.bestPointsText = this.add.bitmapText(
            this.scale.width - 90,
            this.scale.height - 40,
            'pixelFont',
            'MEJOR  ' + this.betsPoints
        ).setDepth(2).setOrigin(0.5);


        // local storage de los mejores puntos
        if (this.points > this.betsPoints) {
            localStorage.setItem('best_points_ninja', this.points);
            // aqui puedes enviarlo a una db
        }

        this.cp.on(Phaser.Input.Events.POINTER_DOWN, () => {
            this.add.tween({
                targets: this.cp,
                ease: 'Bounce.easeIn',
                y: -200,
                duration: 1000,
                onComplete: () => {
                    // this.scene.start('Play');
                    this.scene.start('Playing');
                }
            });
        });
    }
    update(time, delta) {

    }

}
export default Menu;