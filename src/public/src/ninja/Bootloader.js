class Bootloader extends Phaser.Scene {
    constructor() {
        super('Bootloader');
    }

    preload() {
        // Ruta de las imagenes
        this.load.path = '/assets/play4/assets/';

        this.load.image([
            'background',
            'objects1',
            'objects2',
            'ninja',
            'smoke',
            'btntIni',
            // 'trunk',
            // 'tomb',
            // // 'buttonPlay',
            // 'tronco',
            // 'cp',
            // 'title',
            // 'man_hit_marco',
            'btnCAM',
            'btnCEx',
            // 'btnR',
        ]);

        this.load.image('font', '../font/font.png');
        this.load.json('fontData', '../font/font.json');

        this.load.audio('loopMusic', '../sounds/musicLoop.mp3');
        this.load.audio('sfxHit', '../sounds/sfxHit.mp3');
        this.load.audio('sfxLoose', '../sounds/sfxLoose.mp3');

        this.load.atlas('smoks', 'smoks.png', 'smoks_atlas.json');
        this.load.animation('smokAnim', 'smoks_anim.json');

        this.load.on('complete', () => {

            this.sound.play('loopMusic', { loop: true });

            const fontData = this.cache.json.get('fontData');
            this.cache.bitmapFont.add('pixelFont', Phaser.GameObjects.RetroFont.Parse(this, fontData));

            // this.scene.start('Playing');
            this.scene.start('Menu');

        });
    }




}
export default Bootloader;