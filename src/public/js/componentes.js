const anio = new Date().getFullYear();

const a = () => {
    const footer = document.createElement('footer');
    footer.style = 'margin-bottom: 10px; margin-top: 10px;';
    footer.innerHTML = ` &copy; ${anio} copyrigth Funplay`;
    document.body.append(footer);
}

a();

// let stylesF = 'color: rgb(25, 100, 185); background: #1b9bff;';
let stylesF = 'color: white ; background: #1b9bff; transition: .5s;';

// URL http://funplayv1.herokuapp.com/
const urlHome = 'http://localhost:4000/';
const urlHomeP = 'https://funplayv1.herokuapp.com/';
const urlHomePS = 'http://funplayv1.herokuapp.com/';

const urlAbout = 'http://localhost:4000/about';
const urlAboutP = 'https://funplayv1.herokuapp.com/about';
const urlAboutPS = 'http://funplayv1.herokuapp.com/about';

const urlContact = 'http://localhost:4000/contact';
const urlContactP = 'https://funplayv1.herokuapp.com/contact';
const urlContactPS = 'http://funplayv1.herokuapp.com/contact';

const urlPlays = 'http://localhost:4000/plays';
const urlPlaysP = 'https://funplayv1.herokuapp.com/plays';
const urlPlaysPS = 'http://funplayv1.herokuapp.com/plays';
// 'http://funplayv1.herokuapp.com/plays'

const urlNotes = 'http://localhost:4000/notes';
const urlNotesP = 'https://funplayv1.herokuapp.com/notes';
const urlNotesPS = 'http://funplayv1.herokuapp.com/notes';

// Id de la navegacion
const home = document.getElementById('home');
const about = document.getElementById('about');
const contact = document.getElementById('contact');
const plays = document.getElementById('plays');
const notes = document.getElementById('notes');

// d.style.color = 'red';
// d.style = 'color: rgb(25, 100, 185); background: #1b9bff;';
// d.className = 'active';
const url = document.URL;
console.log(url);
if (url) {
    // HOme
    if (url == urlHome || url == urlHomeP || url == urlHomePS) {

        home.style = stylesF;
    } else {
        home.style = '';
    }
    // about
    if (url == urlAbout || url == urlAboutP || url == urlAboutPS) {

        about.style = stylesF;
    } else {
        about.style = '';
    }
    // contact
    if (url == urlContact || url == urlContactP || url == urlContactPS) {

        contact.style = stylesF;
    } else {
        contact.style = '';
    }
    // plays
    if (url == urlPlays || url == urlPlaysP || url == urlPlaysPS) {
        plays.style = stylesF;
    } else {
        // plays.style = '';
    }
    // notes
    if (url == urlNotes || url == urlNotesP || url == urlNotesPS) {
        notes.style = stylesF;
    } else {
        // notes.style = '';
    }
}


// Peticion http post
// function ajax() {
//     const http = new XMLHttpRequest();
//     const url_P = 'http://localhost:5000/grafica-1160b/us-central1/api/juegos/juego1';

//     http.onreadystatechange = () => {
//         if (this.status == 200 && this.readyState == 4) {
//             console.log(this.responseText);
//         }
//     }

//     http.open('POST', url_P);
//     http.send();

// }

// document.getElementById('SEA_HORSE').addEventListener('click', function() {
//     ajax();
// });