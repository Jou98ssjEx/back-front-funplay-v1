const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;
const TwitterStrategy = require('passport-twitter').Strategy;
const User = require('../models/User');
const UserGoogle = require('../models/UserGoogle');
const UserFacebook = require('../models/UserFacebook');

const Keys = require('./pass');

// Serializado Local
passport.serializeUser((user, call) => {
    call(null, user.id);
});

passport.deserializeUser((id, call) => {
    User.findById(id, (err, user) => {
        call(err, user);
    });
});

// Autenticar con Google
passport.use(new GoogleStrategy({
    callbackURL: '/auth/google/redirect',
    clientID: Keys.google.clientID,
    clientSecret: Keys.google.clientSecret
}, (accessToken, refreshToken, profile, call) => {

    const userGoogle = User.findOne({ googleId: profile.id })
        .then((currentUser) => {
            if (currentUser) {
                call(null, currentUser);
                console.log('usuario', currentUser);
            } else {
                // Si el usuario no existe-- crealo
                const googleU = new User({
                        name: profile.displayName,
                        googleId: profile.id,
                        photo: profile.photos[0].value,
                        // email: profile.
                    }).save()
                    .then((newUserGoogle) => {
                        // return newUserGoogle;
                        call(null, newUserGoogle);
                    });
                console.log(googleU);
            }
        });
}));

// Passport con usuario local
passport.use(new LocalStrategy({
    usernameField: 'email'
}, async(email, password, call) => {
    const user = await User.findOne({ email: email });
    if (!user) {
        return call(null, false, {
            message: 'No se encontró el usuario'
        });
    } else {
        const match = await user.matchPassword(password);
        if (match) {
            return call(null, user);
        } else {
            return call(null, false, {
                message: 'Contraseña incoreccta'
            });
        }
    }
}));


// Autenticar con Facebook
passport.use(new FacebookStrategy({
    callbackURL: '/auth/facebook/callback/',
    clientID: Keys.facebook.id,
    clientSecret: Keys.facebook.secret
}, (accessToken, refreshToken, profile, call) => {

    const userGoogle = User.findOne({ facebookId: profile.id })
        .then((currentUser) => {
            if (currentUser) {
                call(null, currentUser);
                console.log('usuario de Face:', currentUser);
            } else {
                // Si el usuario no existe-- crealo
                const googleU = new User({
                        name: profile.displayName,
                        facebookId: profile.id,
                        // photo: profile.photos[0].value,
                        // email: profile.
                    }).save()
                    .then((newUserGoogle) => {
                        // return newUserGoogle;
                        call(null, newUserGoogle);
                    });
                console.log(googleU);
            }
        });
}));
// // Serializado de Google
// passport.serializeUser((userGoogle, cb) => {
//     cb(null, userGoogle.id);
// });

// passport.deserializeUser((id, cb) => {
//     UserGoogle.findById(id).then((userGoogle) => {
//         cb(null, userGoogle);
//     });
// });






// Passport con usuario de Facebook
// passport.use(new FacebookStrategy({
//     callbackURL: '/auth/facebook/callback/',
//     clientID: Keys.facebook.id,
//     clientSecret: Keys.facebook.secret
// }, (accessToken, refreshToken, profile, call) => {
//     console.log(profile);
//     const userFacebook = UserFacebook.findOne({ facebookId: profile.id })
//         .then((currentUserF) => {
//             if (currentUserF) {
//                 call(null, currentUserF);
//                 console.log('usuario de Face:', currentUserF);
//             } else {
//                 new UserFacebook({
//                         username: profile.displayName,
//                         facebookId: profile.id,
//                     }).save()
//                     .then((newUserFacebook) => {
//                         call(null, newUserFacebook);
//                         console.log(newUserFacebook);
//                     });
//             }
//         });
// }));

// Serializado de Facebook
// passport.serializeUser((userFacebook, callback) => {
//     callback(null, userFacebook.id);
// });

// passport.deserializeUser((id, call) => {
//     UserFacebook.findById(id).then((userFacebook) => {
//         call(null, userFacebook);
//     });
// });



// Passport con usuario de Twitter