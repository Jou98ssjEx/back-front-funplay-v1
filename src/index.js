const express = require('express');
const path = require('path');
const flash = require('connect-flash');
const session = require('express-session');
const passport = require('passport');
const method = require('method-override');
const exphbs = require('express-handlebars');
const { allowInsecurePrototypeAccess } = require('@handlebars/allow-prototype-access');
const Handlebars = require('handlebars');

// Initiliazations
const app = express();
require('./database');
require('./config/password');

// Settings
app.set('port', process.env.PORT || 4000);

app.set('views', path.join(__dirname, 'views')); // Se define la ruta de las vistas
app.engine('hbs', exphbs({ // Config para las vistas
    defaultLayout: 'main',
    layoutsDir: path.join(app.get('views'), 'layouts'), // Une las vistas con los layout
    partialsDir: path.join(app.get('views'), 'partials'), // Une las vistas con partial
    handlebars: allowInsecurePrototypeAccess(Handlebars),
    extname: '.hbs'
}));
app.set('view engine', '.hbs');

// Middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(method('_method'));
app.use(session({
    secret: 'myapp',
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());


// Global Var
app.use((req, res, next) => {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    res.locals.user = req.user || null;
    next();
});

// Routes
app.use(require('./routes/index'));
app.use(require('./routes/users'));
app.use(require('./routes/plays'));
app.use(require('./routes/notes'));
app.use(require('./routes/email'));
app.use(require('./routes/startPlay'));


// Static File
app.use(express.static(path.join(__dirname, 'public')));


// Server is listenning
app.listen(app.get('port'), () => {
    console.log('Server Online in port', app.get('port'));
});