const mongoose = require('mongoose');
const { Schema } = mongoose;
const bycry = require('bcryptjs');

const UserFacebookSchema = new Schema({
    username: {
        type: String,

    },
    facebookId: {
        type: String,

    },
    photo: {
        type: String,

    },
    email: {
        type: String,

    },
    // email: {
    //     type: String,

    // },
    // password: {
    //     type: String,

    // },
    date: {
        type: Date,
        default: Date.now
    },
});

// UserSchema.methods.encryptPassword = async(password) => {
//     const salt = await bycry.genSalt(10);
//     const hash = bycry.hash(password, salt);
//     return hash;
// }

// UserSchema.methods.matchPassword = async function(password) {
//     return await bycry.compare(password, this.password);
// }
module.exports = mongoose.model('UserFacebook', UserFacebookSchema);