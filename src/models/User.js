const mongoose = require('mongoose');
const { Schema } = mongoose;
const bycry = require('bcryptjs');

const UserSchema = new Schema({
    name: {
        type: String,
        // required: true
    },
    email: {
        type: String,
        // required: true
    },
    password: {
        type: String,
        // required: true
    },
    googleId: {
        type: String,

    },
    facebookId: {
        type: String,

    },
    photo: {
        type: String,

    },
    date: {
        type: Date,
        default: Date.now
    },
});

UserSchema.methods.encryptPassword = async(password) => {
    const salt = await bycry.genSalt(10);
    const hash = bycry.hash(password, salt);
    return hash;
}

UserSchema.methods.matchPassword = async function(password) {
    return await bycry.compare(password, this.password);
}
module.exports = mongoose.model('User', UserSchema);