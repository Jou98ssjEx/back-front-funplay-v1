const mongoose = require('mongoose');
const { Schema } = mongoose;
const bycry = require('bcryptjs');

const UserSocialSchema = new Schema({
    name: {
        type: String,
    },
    email: {
        type: String,
    },
    googleId: {
        type: String,

    },
    facebookId: {
        type: String,

    },
    photo: {
        type: String,

    },
    date: {
        type: Date,
        default: Date.now
    },
});


module.exports = mongoose.model('UserSocial', UserSocialSchema);