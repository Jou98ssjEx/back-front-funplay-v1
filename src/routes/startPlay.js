const express = require('express');
const router = express.Router();

// const { isAuthenticated } = require('../helpers/auth');

// Sea Horse
router.get('/start/startPlay', (req, res) => {
    res.render('./start/horse');
});
// Backjack
router.get('/start/startPlay1', (req, res) => {
    res.render('./start/TicTacToe');
});
// leñador
router.get('/start/startPlay2', (req, res) => {
    res.render('./start/lenador');
});
// ninja
router.get('/start/startPlay3', (req, res) => {
    res.render('./start/ninja');
});
// pinguino
router.get('/start/startPlay4', (req, res) => {
    res.render('./start/pinguino');
});
// ceresita
router.get('/start/startPlay5', (req, res) => {
    res.render('./start/Tom');
});
// incognito
router.get('/start/startPlay6', (req, res) => {
    res.render('./start/incognito');
});



// router.get('/start', (req, res) => {
//     res.render('start');
// });
// router.get('/plays', isAuthenticated, (req, res) => {
//     res.render('plays');
// });
// router.get('/about', (req, res) => {
//     res.render('about');
// });
// router.get('/contact', (req, res) => {
//     res.render('contact');
// });
// router.get('/prueba', (req, res) => {
//     res.render('prueba');
// });


module.exports = router;