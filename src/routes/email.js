const express = require('express');
const router = express.Router();

// Requerir model back
const Email = require('../models/Email');

router.post('/send-email', async(req, res) => {
    const { fullname, email, phone, affair, message } = req.body;

    const errors = [];

    if (!fullname) {
        errors.push({
            text: 'Por favor insertar su Nombre'
        });
    }
    if (!email) {
        errors.push({
            text: 'Por favor insertar su email'
        });
    }
    if (!phone) {
        errors.push({
            text: 'Por favor insertar su número de telefono'
        });
    }
    if (!affair) {
        errors.push({
            text: 'Por favor insertar su asunto'
        });
    }
    if (!message) {
        errors.push({
            text: 'Por favor insertar una descripción'
        });
    }
    if (errors.length > 0) {
        res.render('/contact', {
            errors,
            fullname,
            email,
            phone,
            affair,
            message

        });
    } else {
        const newEmail = new Email({
            fullname,
            email,
            phone,
            affair,
            message
        });
        await newEmail.save();
        req.flash('success_msg', 'Se Envió el formulario de Contacto');
        res.redirect('/contact');
    }
    // res.send('ok')
});

module.exports = router;