const express = require('express');
const router = express.Router();

const { isAuthenticated } = require('../helpers/auth');

// Requerir model back
const Note = require('../models/Note');

router.get('/notes/add', isAuthenticated, (req, res) => {
    res.render('notes/new-note');
});




// Rutas de back

router.post('/notes/new-note', async(req, res) => {
    const { title, description } = req.body;
    const errors = [];

    if (!title) {
        errors.push({
            text: 'Por favor insertar un Título'
        });
    }
    if (!description) {
        errors.push({
            text: 'Por favor insertar una descripción'
        });
    }
    if (errors.length > 0) {
        res.render('notes/new-note', {
            errors,
            title,
            description
        });
    } else {
        const newNotes = new Note({
            title,
            description
        });
        // Esta line me permite saber el enlace de la nota con el user
        newNotes.user = req.user.id;
        await newNotes.save();
        req.flash('success_msg', 'Se registro la nota');
        res.redirect('/notes');
    }
});

// Esta ruta va a recibir notas de la db
router.get('/notes', isAuthenticated, async(req, res) => {
    const notes = await Note.find({ user: req.user.id }).sort({ date: 'desc' });
    // Con esto va a renderizar la pag y pasarle las notas
    res.render('notes/all-notes', { notes });
});


// Esta ruta te permite traer la vista para editar, debe llamar a un formulario

router.get('/notes/edit/:id', isAuthenticated, async(req, res) => {
    const note = await Note.findById(req.params.id);
    res.render('notes/edit-note', { note });
});

// Esta ruta hace la peticion put para actualizar la db

router.put('/notes/edit-note/:id', isAuthenticated, async(req, res) => {
    const { title, description } = req.body;
    await Note.findByIdAndUpdate(req.params.id, { title, description });
    req.flash('success_msg', 'La nota se actualizó correctamente');
    res.redirect('/notes');
});

// para eliminar la nota

router.delete('/notes/delete/:id', isAuthenticated, async(req, res) => {
    await Note.findByIdAndDelete(req.params.id);
    req.flash('success_msg', 'La nota se eliminó correctamente');
    res.redirect('/notes');
});

module.exports = router;