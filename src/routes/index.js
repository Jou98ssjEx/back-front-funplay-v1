const express = require('express');
const router = express.Router();

const { isAuthenticated } = require('../helpers/auth');

router.get('/', (req, res) => {
    res.render('index');
});
router.get('/start', (req, res) => {
    res.render('start');
});
router.get('/plays', isAuthenticated, (req, res) => {
    res.render('plays');
});
router.get('/about', (req, res) => {
    res.render('about');
});
router.get('/contact', (req, res) => {
    res.render('contact');
});
router.get('/prueba', (req, res) => {
    res.render('prueba');
});
router.get('/grafica', (req, res) => {
    res.render('grafica');
});


module.exports = router;