const express = require('express');
const router = express.Router();

router.get('/plays/juego1', (req, res) => {
    res.render('./plays/juego1');
});
router.get('/plays/juego2', (req, res) => {
    res.render('./plays/juego2');
});
router.get('/plays/juego3', (req, res) => {
    res.render('./plays/juego3');
});


module.exports = router;